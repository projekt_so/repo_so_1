#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <queue>

int cityA=0, cityB=0, waitA=0, waitB=0;

typedef struct car{
    int nr;
    bool site; 
}car_t;

typedef struct bridge {
    int cars;
    pthread_mutex_t mutex;
} bridge_t;
static bridge_t bridge={
    bridge.cars = 0,
    pthread_mutex_init(&bridge.mutex, NULL)};

struct std::queue<int> queue;

/*arrive - funkcja odpowiadajaca za obsluge samochodow chcacych przejachac przez most,
 blokuje samochody przed mostem gdy na moscie znajduje sie auto*/
static void arrive(bridge_t *bridge, car_t *car)
{
    pthread_mutex_lock(&bridge->mutex);
    queue.push(car->nr);
    if( car->site ){
        cityB-=1;
        waitB+=1;
    }else{
        cityA-=1;
        waitA+=1;
    }
    pthread_mutex_unlock(&bridge->mutex);

     while (bridge->cars != 0 || queue.front() != car->nr) {
    }
    pthread_mutex_lock(&bridge->mutex);

    bridge->cars++;
    pthread_mutex_unlock(&bridge->mutex);
}

/*cross - funkcja odpowiadacjaca za przejazd auta przez most*/
static void cross(bridge_t *bridge, car_t *car)
{
    if(!car->site)
        printf("%d : %d >> %d >> %d : %d\n", cityA, waitA, car->nr, waitB, cityB);
    else
        printf("%d : %d << %d << %d : %d\n", cityA, waitA, car->nr, waitB, cityB);

    sleep(1);
}

/*leave - funkcja odpowiadajaca za obsluge samochodow wyjezdzajacych z mostu,
po wyjechaniu jednego auta udostepnia most dla innego auta*/
static void leave(bridge_t *bridge, car_t *car)
{
    pthread_mutex_lock(&bridge->mutex);
    car->site= !car->site;
    if( car->site ){
        cityB+=1;
        waitA-=1;
    }else{
        cityA+=1;
        waitB-=1;
    }
    queue.pop();
    bridge->cars--;
    pthread_mutex_unlock(&bridge->mutex);
    sleep(10);
}

/*drive - funkcja wykonuje proces przejechania auta przez most*/
static void drive(bridge_t *bridge, car_t *car)
{
        arrive(bridge, car);
        cross(bridge, car);
        leave(bridge, car);
}

/*goTo - jest to funkcja startowa dla watkow  */
static void *goTo(void *data)
{
    car_t car = *(car_t *)data;
    while(1){
        drive(&bridge, &car);
    }
    return NULL;
}

/*run - funkcja tworzy watki i uruchamia na nich funkcje goTo */
static int run(int n)
{
    int i;
    pthread_t thread[n];
    car_t tab[n];
    for(i=0;i<n;i++){
        tab[i].nr = i;
        if(i<n/2){
            tab[i].site = false;
            cityA++;
        }else{
            tab[i].site = true;
            cityB++;
        }
    }
        for (i = 0; i < n; i++) {
        if (pthread_create(&thread[i], NULL, goTo ,&tab[i])) {
        printf("Thread creation failed\n");
        return EXIT_FAILURE;
    }
    }
    for (i = 0; i < n; i++) {
        if (thread[i]) pthread_join(thread[i], NULL);
    }
    return EXIT_SUCCESS;
}

int main(int argc, char **argv)
{
    if( argc > 2 || argc < 2 )
   	{
     	printf("Wrong number of arguments\n");
      	exit(EXIT_FAILURE);
   	}
    int n = atoi(argv[1]);
    if(n < 1){
        printf("Wrong argument\n");
      	exit(EXIT_FAILURE);
    }
    return run(n);
}